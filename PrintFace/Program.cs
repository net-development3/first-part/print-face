﻿using System;

namespace PrintFace
{
    public static class Program
    {
        public static void Main()
        {
            Console.WriteLine("Hello, world!");
        }

        public static void SayHelloUser(string userName)
        {
            string name = userName;
            Console.WriteLine("Hello, " + name + "!");
        }

        public static void PrintFace()
        {
            Console.WriteLine(" +\"\"\"\"\"+");
            Console.WriteLine("(| o o |)");
            Console.WriteLine(" |  ^  |");
            Console.WriteLine(" | '-' |");
            Console.WriteLine(" +-----+");
        }
    }
}
